
# Music Playlist API Documentation

This documentation provides details about the RESTful endpoints in the Music Playlist application, covering Album, Genre, and Song controllers.

## AlbumController

### Endpoints

- **GET `/api/v1/album/all`**
  - Description: Retrieves a list of all albums.
  - No request parameters.

- **GET `/api/v1/album/search/{query}`**
  - Description: Searches for albums based on a query string (album or singer).
  - Path variable: `query` (String) - The search query.

- **POST `/api/v1/album/add`**
  - Description: Adds a new album.
  - Request body: `Album` object in JSON format.

- **PUT `/api/v1/album/update/{id}`**
  - Description: Updates an existing album identified by its UUID.
  - Path variable: `id` (UUID) - Unique identifier of the album.
  - Request body: `Album` object in JSON format.

- **POST `/api/v1/album/delete/{id}`**
  - Description: Deletes an album identified by its UUID.
  - Path variable: `id` (UUID) - Unique identifier of the album.

## GenreController

### Endpoints

- **GET `/api/v1/genre/all`**
  - Description: Retrieves a list of all genres.
  - No request parameters.

- **POST `/api/v1/genre/add`**
  - Description: Adds a new genre.
  - Request body: `Genre` object in JSON format.

- **PUT `/api/v1/genre/update/{id}`**
  - Description: Updates an existing genre identified by its ID.
  - Path variable: `id` (Long) - Unique identifier of the genre.
  - Request body: `Genre` object in JSON format.

- **GET `/api/v1/genre/delete/{id}`**
  - Description: Deletes a genre identified by its ID.
  - Path variable: `id` (Long) - Unique identifier of the genre.

## SongController

### Endpoints

- **GET `/api/v1/song/allSongs`**
  - Description: Retrieves all songs.
  - No request parameters.

- **POST `/api/v1/song/add`**
  - Description: Adds a new song.
  - Request body: `Song` object in JSON format.

- **GET `/api/v1/song/{id}`**
  - Description: Retrieves a specific song by its UUID.
  - Path variable: `id` (UUID) - Unique identifier of the song.

- **GET `/api/v1/song/all`**
  - Description: Retrieves songs by album ID if provided; otherwise, returns all songs.
  - Request parameter: `albumId` (UUID) - Optional. Unique identifier of the album.

- **PUT `/api/v1/song/update/{id}`**
  - Description: Updates a song identified by its UUID.
  - Path variable: `id` (UUID) - Unique identifier of the song.
  - Request body: `Song` object in JSON format.

- **POST `/api/v1/song/delete/{id}`**
  - Description: Deletes a song identified by its UUID.
  - Path variable: `id` (UUID) - Unique identifier of the song.

# Screenshots

![Screenshot-1](Screenshot1.png)