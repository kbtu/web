export default {
    songs: {
    "properties": {},
    tracks: [
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "633367735",
        "title": "Si No Estás",
        "subtitle": "Iñigo Quintero",
        "share": {
          "subject": "Si No Estás - Iñigo Quintero",
          "text": "Si No Estás by Iñigo Quintero",
          "href": "https://www.shazam.com/track/633367735/si-no-est%C3%A1s",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/c4/b2/f2/c4b2f2ae-5122-d699-2191-d6b438ed199f/cover.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Si No Estás by Iñigo Quintero.",
          "html": "https://www.shazam.com/snippets/email-share/633367735?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/c7/6e/49/c76e49ea-cafd-13e5-7fbd-1ee2e9fe8a63/48a115d8-5378-4dc9-b5a3-7e4eb08169cc_file_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/633367735"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/c7/6e/49/c76e49ea-cafd-13e5-7fbd-1ee2e9fe8a63/48a115d8-5378-4dc9-b5a3-7e4eb08169cc_file_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/c4/b2/f2/c4b2f2ae-5122-d699-2191-d6b438ed199f/cover.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/c4/b2/f2/c4b2f2ae-5122-d699-2191-d6b438ed199f/cover.jpg/400x400cc.jpg",
          "joecolor": "b:e7e7e7p:000000s:1b1b1bt:2e2e2eq:434343"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1678217896"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/58/31/ed/5831ede0-611c-479c-ea3b-4e646066e4b8/mzaf_7161969819350161171.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/si-no-est%C3%A1s/1678217704?i=1678217896&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/si-no-est%C3%A1s/1678217704?i=1678217896&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "i%C3%B1igo-quintero",
            "id": "42",
            "adamid": "1498499723"
          }
        ],
        "url": "https://www.shazam.com/track/633367735/si-no-est%C3%A1s",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1498499723/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "675031499",
        "title": "greedy",
        "subtitle": "Tate McRae",
        "share": {
          "subject": "greedy - Tate McRae",
          "text": "greedy by Tate McRae",
          "href": "https://www.shazam.com/track/675031499/greedy",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/23/07/92/23079247-25be-3098-ef53-78e7d0fe7406/196871341653.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover greedy by Tate McRae.",
          "html": "https://www.shazam.com/snippets/email-share/675031499?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/29/18/b6/2918b602-4505-0632-73cf-5226f9581a87/fda79ffd-5fd1-40b3-8bd7-e8ea5531dff7_ami-identity-11b6c33ddb521791b7ba1dd8c4dcf764-2023-09-15T05-27-44.467Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/675031499"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/29/18/b6/2918b602-4505-0632-73cf-5226f9581a87/fda79ffd-5fd1-40b3-8bd7-e8ea5531dff7_ami-identity-11b6c33ddb521791b7ba1dd8c4dcf764-2023-09-15T05-27-44.467Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/23/07/92/23079247-25be-3098-ef53-78e7d0fe7406/196871341653.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/23/07/92/23079247-25be-3098-ef53-78e7d0fe7406/196871341653.jpg/400x400cc.jpg",
          "joecolor": "b:fefefep:090509s:242423t:3a373aq:4f504f"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1706381103"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/1a/92/61/1a92611d-d5df-c0e1-59f6-63a89297244f/mzaf_10360959294085002392.plus.aac.p.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/greedy/1706381100?i=1706381103&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/greedy/1706381100?i=1706381103&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "tate-mcrae",
            "id": "42",
            "adamid": "1446365464"
          }
        ],
        "url": "https://www.shazam.com/track/675031499/greedy",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1446365464/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1707092755",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1707092755?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "673557639",
        "title": "Paint The Town Red",
        "subtitle": "Doja Cat",
        "share": {
          "subject": "Paint The Town Red - Doja Cat",
          "text": "Paint The Town Red by Doja Cat",
          "href": "https://www.shazam.com/track/673557639/paint-the-town-red",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/97/c7/f2/97c7f256-6db0-a45b-a786-f58dc928c970/196871404495.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Paint The Town Red by Doja Cat.",
          "html": "https://www.shazam.com/snippets/email-share/673557639?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/54/ba/99/54ba9945-5aae-bec7-a80c-188de630b37b/d62857dc-e8bd-41f6-b4d5-e368875c6044_ami-identity-7d4c30a95a90f129a834aecd444aa07a-2023-06-16T14-02-41.627Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/673557639"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/54/ba/99/54ba9945-5aae-bec7-a80c-188de630b37b/d62857dc-e8bd-41f6-b4d5-e368875c6044_ami-identity-7d4c30a95a90f129a834aecd444aa07a-2023-06-16T14-02-41.627Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/97/c7/f2/97c7f256-6db0-a45b-a786-f58dc928c970/196871404495.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/97/c7/f2/97c7f256-6db0-a45b-a786-f58dc928c970/196871404495.jpg/400x400cc.jpg",
          "joecolor": "b:ffffffp:050607s:1b1515t:373838q:494443"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1704921697"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/cd/87/0b/cd870b5f-c356-f761-e176-c45aba37eab1/mzaf_17565732451070337798.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/paint-the-town-red/1704921687?i=1704921697&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/paint-the-town-red/1704921687?i=1704921697&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "doja-cat",
            "id": "42",
            "adamid": "830588310"
          }
        ],
        "url": "https://www.shazam.com/track/673557639/paint-the-town-red",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/830588310/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1701008023",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1701008023?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "675132421",
        "title": "Strangers",
        "subtitle": "Kenya Grace",
        "share": {
          "subject": "Strangers - Kenya Grace",
          "text": "Strangers by Kenya Grace",
          "href": "https://www.shazam.com/track/675132421/strangers",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/82/da/f7/82daf7a1-4d0e-9a94-9314-0916e4ab5635/054391385239.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Strangers by Kenya Grace.",
          "html": "https://www.shazam.com/snippets/email-share/675132421?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/12/88/5a/12885a75-f1bc-e71d-bbd6-b357d5f2e6b4/31f61f5b-94ac-48ab-8ea9-4c2c7fc75369_ami-identity-9071226a01f789ae6bfa56e1c87e1a22-2023-04-26T13-25-58.614Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/675132421"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/12/88/5a/12885a75-f1bc-e71d-bbd6-b357d5f2e6b4/31f61f5b-94ac-48ab-8ea9-4c2c7fc75369_ami-identity-9071226a01f789ae6bfa56e1c87e1a22-2023-04-26T13-25-58.614Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/82/da/f7/82daf7a1-4d0e-9a94-9314-0916e4ab5635/054391385239.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/82/da/f7/82daf7a1-4d0e-9a94-9314-0916e4ab5635/054391385239.jpg/400x400cc.jpg",
          "joecolor": "b:000a19p:aacf3bs:d1a9b0t:88a834q:a78992"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1704393613"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/85/5b/b5/855bb54a-f6ca-2b11-22ad-3c9ac15bd019/mzaf_5684878710251751871.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/strangers/1704393598?i=1704393613&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/strangers/1704393598?i=1704393613&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "kenya-grace",
            "id": "42",
            "adamid": "1476584041"
          }
        ],
        "url": "https://www.shazam.com/track/675132421/strangers",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1476584041/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "673104339",
        "title": "Water",
        "subtitle": "Tyla",
        "share": {
          "subject": "Water - Tyla",
          "text": "Water by Tyla",
          "href": "https://www.shazam.com/track/673104339/water",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/1e/c9/5b/1ec95bbc-a56f-cbef-070c-ddec9f4fdd3c/196871296205.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Water by Tyla.",
          "html": "https://www.shazam.com/snippets/email-share/673104339?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/29/46/66/294666e7-cce0-ede3-fb48-cdf2999c96da/0e948386-5d27-46db-ab40-8b545db0ed38_ami-identity-a66d9229f871152b4ff92342246646ef-2023-07-27T20-30-43.526Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/673104339"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/29/46/66/294666e7-cce0-ede3-fb48-cdf2999c96da/0e948386-5d27-46db-ab40-8b545db0ed38_ami-identity-a66d9229f871152b4ff92342246646ef-2023-07-27T20-30-43.526Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/1e/c9/5b/1ec95bbc-a56f-cbef-070c-ddec9f4fdd3c/196871296205.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/1e/c9/5b/1ec95bbc-a56f-cbef-070c-ddec9f4fdd3c/196871296205.jpg/400x400cc.jpg",
          "joecolor": "b:fcf4f1p:090809s:4a2c1bt:393737q:6e5446"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1699082734"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview126/v4/a8/71/54/a8715458-d69b-3822-ba75-ab45d40f5ce2/mzaf_9234836699925288230.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/water/1699082722?i=1699082734&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/water/1699082722?i=1699082734&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "tyla",
            "id": "42",
            "adamid": "1552925732"
          }
        ],
        "url": "https://www.shazam.com/track/673104339/water",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1552925732/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1710629437",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1710629437?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "667312288",
        "title": "(It Goes Like) Nanana (Edit)",
        "subtitle": "Peggy Gou",
        "share": {
          "subject": "(It Goes Like) Nanana (Edit) - Peggy Gou",
          "text": "(It Goes Like) Nanana (Edit) by Peggy Gou",
          "href": "https://www.shazam.com/track/667312288/it-goes-like-nanana-edit",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/a7/a7/96/a7a79669-7b20-9026-cb8e-773b1a9d31e3/191404137420.png/400x400cc.jpg",
          "twitter": "I used @Shazam to discover (It Goes Like) Nanana (Edit) by Peggy Gou.",
          "html": "https://www.shazam.com/snippets/email-share/667312288?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/3a/de/39/3ade39e8-6de7-ee82-9c02-b9da415205b5/70ea60b1-f26f-4c7d-baa1-c7f969418a03_ami-identity-31a954c200a5c5c449bf5faedb6bd2b7-2023-06-16T08-51-47.675Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/667312288"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/3a/de/39/3ade39e8-6de7-ee82-9c02-b9da415205b5/70ea60b1-f26f-4c7d-baa1-c7f969418a03_ami-identity-31a954c200a5c5c449bf5faedb6bd2b7-2023-06-16T08-51-47.675Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/a7/a7/96/a7a79669-7b20-9026-cb8e-773b1a9d31e3/191404137420.png/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/a7/a7/96/a7a79669-7b20-9026-cb8e-773b1a9d31e3/191404137420.png/400x400cc.jpg",
          "joecolor": "b:c8c17bp:12151bs:281f21t:37372eq:483f33"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1689088958"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview126/v4/6a/a1/29/6aa129cb-d7d9-3f66-d2c3-b9aff26705a3/mzaf_4261627486215746711.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/it-goes-like-nanana-edit/1689088940?i=1689088958&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/it-goes-like-nanana-edit/1689088940?i=1689088958&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "peggy-gou",
            "id": "42",
            "adamid": "1068912248"
          }
        ],
        "url": "https://www.shazam.com/track/667312288/it-goes-like-nanana-edit",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1068912248/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1709503187",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1709503187?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "673554337",
        "title": "Vois sur ton chemin (Techno Mix)",
        "subtitle": "BENNETT",
        "share": {
          "subject": "Vois sur ton chemin (Techno Mix) - BENNETT",
          "text": "Vois sur ton chemin (Techno Mix) by BENNETT",
          "href": "https://www.shazam.com/track/673554337/vois-sur-ton-chemin-techno-mix",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/92/68/cc/9268cc08-e44e-16f5-5682-e769e3d97f9f/5054197770722.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Vois sur ton chemin (Techno Mix) by BENNETT.",
          "html": "https://www.shazam.com/snippets/email-share/673554337?lang=en-US&country=US",
          "snapchat": "https://www.shazam.com/partner/sc/track/673554337"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/92/68/cc/9268cc08-e44e-16f5-5682-e769e3d97f9f/5054197770722.jpg/400x400cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/92/68/cc/9268cc08-e44e-16f5-5682-e769e3d97f9f/5054197770722.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/92/68/cc/9268cc08-e44e-16f5-5682-e769e3d97f9f/5054197770722.jpg/400x400cc.jpg",
          "joecolor": "b:1e1e1ep:ffe401s:d2ba79t:d1bc07q:ae9a67"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1699825323"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview126/v4/69/0b/b3/690bb3ec-cda5-cf2f-c86d-03c98c48db9a/mzaf_16116283004972729891.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/vois-sur-ton-chemin-techno-mix/1699825309?i=1699825323&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/vois-sur-ton-chemin-techno-mix/1699825309?i=1699825323&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "bennett",
            "id": "42",
            "adamid": "1445044753"
          }
        ],
        "url": "https://www.shazam.com/track/673554337/vois-sur-ton-chemin-techno-mix",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1445044753/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "672591037",
        "title": "Le Monde (From Talk to Me)",
        "subtitle": "Richard Carter",
        "share": {
          "subject": "Le Monde (From Talk to Me) - Richard Carter",
          "text": "Le Monde (From Talk to Me) by Richard Carter",
          "href": "https://www.shazam.com/track/672591037/le-monde-from-talk-to-me",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/c0/52/16/c05216ac-98f2-4b83-8bf8-cdf50e8ce6ee/41711.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Le Monde (From Talk to Me) by Richard Carter.",
          "html": "https://www.shazam.com/snippets/email-share/672591037?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/bd/8a/47/bd8a47a8-bdb5-46b3-9fae-22a92dfaa7d2/5c98b77d-050f-45c1-96db-5f26fa426f5c_file_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/672591037"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/bd/8a/47/bd8a47a8-bdb5-46b3-9fae-22a92dfaa7d2/5c98b77d-050f-45c1-96db-5f26fa426f5c_file_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/c0/52/16/c05216ac-98f2-4b83-8bf8-cdf50e8ce6ee/41711.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/c0/52/16/c05216ac-98f2-4b83-8bf8-cdf50e8ce6ee/41711.jpg/400x400cc.jpg",
          "joecolor": "b:5b5b5bp:fefefes:f5f5f5t:ddddddq:d6d6d6"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1707436042"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/b3/9d/be/b39dbeb5-8a99-3f2a-ef6b-c011f7e1f825/mzaf_3302117330170446581.plus.aac.p.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/le-monde-from-talk-to-me/1707436041?i=1707436042&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/le-monde-from-talk-to-me/1707436041?i=1707436042&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "richard-carter",
            "id": "42",
            "adamid": "1449275169"
          }
        ],
        "url": "https://www.shazam.com/track/672591037/le-monde-from-talk-to-me",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1449275169/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "679088742",
        "title": "Dalie (feat. Baby S.O.N)",
        "subtitle": "Kamo Mphela, Tyler ICU & Khalil Harrison",
        "share": {
          "subject": "Dalie (feat. Baby S.O.N) - Kamo Mphela, Tyler ICU & Khalil Harrison",
          "text": "Dalie (feat. Baby S.O.N) by Kamo Mphela, Tyler ICU & Khalil Harrison",
          "href": "https://www.shazam.com/track/679088742/dalie-feat-baby-s-o-n",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/10/0f/34/100f3465-7a53-07b2-88d3-ced519490bb3/0766214657743.png/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Dalie (feat. Baby S.O.N) by Kamo Mphela, Tyler ICU & Khalil Harrison.",
          "html": "https://www.shazam.com/snippets/email-share/679088742?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/5a/7a/47/5a7a47f7-fa43-4bae-de0b-d931b27ca9fd/5c5b4ee3-a2b0-4cef-b337-975177b5b9d7_ami-identity-753dea192386d5fb18bda6011ef987f2-2023-09-18T16-34-12.100Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/679088742"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/5a/7a/47/5a7a47f7-fa43-4bae-de0b-d931b27ca9fd/5c5b4ee3-a2b0-4cef-b337-975177b5b9d7_ami-identity-753dea192386d5fb18bda6011ef987f2-2023-09-18T16-34-12.100Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/10/0f/34/100f3465-7a53-07b2-88d3-ced519490bb3/0766214657743.png/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/10/0f/34/100f3465-7a53-07b2-88d3-ced519490bb3/0766214657743.png/400x400cc.jpg",
          "joecolor": "b:010101p:f4f5f9s:d0cbc7t:c3c4c7q:a7a29f"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1710560833"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/e2/d8/17/e2d817fa-7449-c538-24c0-cda5b90814ac/mzaf_3965389837170077766.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/dalie-feat-baby-s-o-n/1710560831?i=1710560833&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/dalie-feat-baby-s-o-n/1710560831?i=1710560833&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "kamo-mphela",
            "id": "42",
            "adamid": "1471723947"
          },
          {
            "alias": "tyler-icu",
            "id": "42",
            "adamid": "1408283553"
          },
          {
            "alias": "khalil-harrison",
            "id": "42",
            "adamid": "1554719458"
          }
        ],
        "url": "https://www.shazam.com/track/679088742/dalie-feat-baby-s-o-n",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1471723947/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "653369691",
        "title": "Tattoo",
        "subtitle": "Loreen",
        "share": {
          "subject": "Tattoo - Loreen",
          "text": "Tattoo by Loreen",
          "href": "https://www.shazam.com/track/653369691/tattoo",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/50/32/fd/5032fddc-e486-956d-a503-7ec6d17af848/22UM1IM46463.rgb.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Tattoo by Loreen.",
          "html": "https://www.shazam.com/snippets/email-share/653369691?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/c0/56/8e/c0568e8e-2880-5ecb-23e8-3c802ea24221/f2f9b0f7-de4f-4047-b1bb-5b7b9292b887_ami-identity-ebd31eb1e543e630fcd5a70d51ee8e60-2023-10-14T13-43-37.670Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/653369691"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/c0/56/8e/c0568e8e-2880-5ecb-23e8-3c802ea24221/f2f9b0f7-de4f-4047-b1bb-5b7b9292b887_ami-identity-ebd31eb1e543e630fcd5a70d51ee8e60-2023-10-14T13-43-37.670Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/50/32/fd/5032fddc-e486-956d-a503-7ec6d17af848/22UM1IM46463.rgb.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/50/32/fd/5032fddc-e486-956d-a503-7ec6d17af848/22UM1IM46463.rgb.jpg/400x400cc.jpg",
          "joecolor": "b:3c3c3cp:f6f6f6s:e2e9f0t:d0d0d0q:c0c6cc"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1672056675"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview126/v4/28/a3/3e/28a33e92-39f5-ba72-9b18-bba16f9b36de/mzaf_9634311354149611886.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/tattoo/1672056674?i=1672056675&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/tattoo/1672056674?i=1672056675&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "loreen",
            "id": "42",
            "adamid": "417755574"
          }
        ],
        "url": "https://www.shazam.com/track/653369691/tattoo",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/417755574/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "670453090",
        "title": "vampire",
        "subtitle": "Olivia Rodrigo",
        "share": {
          "subject": "vampire - Olivia Rodrigo",
          "text": "vampire by Olivia Rodrigo",
          "href": "https://www.shazam.com/track/670453090/vampire",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/9b/d8/9c/9bd89c9e-b44d-ad25-1516-b9b30f64fd2a/23UMGIM71510.rgb.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover vampire by Olivia Rodrigo.",
          "html": "https://www.shazam.com/snippets/email-share/670453090?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/34/dd/36/34dd3678-40c6-9d8b-fa0f-cb6d82d3103b/6c46071d-e4d2-4671-a110-6ad83a63b89b_ami-identity-5b05d351a6e3c7256ef7680c8aef2894-2023-06-30T03-58-10.754Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/670453090"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/34/dd/36/34dd3678-40c6-9d8b-fa0f-cb6d82d3103b/6c46071d-e4d2-4671-a110-6ad83a63b89b_ami-identity-5b05d351a6e3c7256ef7680c8aef2894-2023-06-30T03-58-10.754Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/9b/d8/9c/9bd89c9e-b44d-ad25-1516-b9b30f64fd2a/23UMGIM71510.rgb.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/9b/d8/9c/9bd89c9e-b44d-ad25-1516-b9b30f64fd2a/23UMGIM71510.rgb.jpg/400x400cc.jpg",
          "joecolor": "b:14143bp:e39893s:d0948ct:ba7d81q:aa7b7c"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1694386830"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview126/v4/eb/c2/e4/ebc2e4fc-2645-d745-46bd-98aa65f146bd/mzaf_14489812662038341591.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/vampire/1694386825?i=1694386830&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/vampire/1694386825?i=1694386830&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "olivia-rodrigo",
            "id": "42",
            "adamid": "979458609"
          }
        ],
        "url": "https://www.shazam.com/track/670453090/vampire",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/979458609/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1694610583",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1694610583?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "668616600",
        "title": "Prada",
        "subtitle": "cassö, RAYE & D-Block Europe",
        "share": {
          "subject": "Prada - cassö, RAYE & D-Block Europe",
          "text": "Prada by cassö, RAYE & D-Block Europe",
          "href": "https://www.shazam.com/track/668616600/prada",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/81/59/f7/8159f7b5-52c0-003b-92c2-04c98a6147a1/196871373258.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Prada by cassö, RAYE & D-Block Europe.",
          "html": "https://www.shazam.com/snippets/email-share/668616600?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/94/5f/7c/945f7c49-26ce-c2f8-2932-f79ce67acdfa/f0600603-d695-44c7-87b6-3a53c67dc774_ami-identity-898521b78a523adefa794ae2ef9f186c-2023-08-17T20-24-04.528Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/668616600"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/94/5f/7c/945f7c49-26ce-c2f8-2932-f79ce67acdfa/f0600603-d695-44c7-87b6-3a53c67dc774_ami-identity-898521b78a523adefa794ae2ef9f186c-2023-08-17T20-24-04.528Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/81/59/f7/8159f7b5-52c0-003b-92c2-04c98a6147a1/196871373258.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/81/59/f7/8159f7b5-52c0-003b-92c2-04c98a6147a1/196871373258.jpg/400x400cc.jpg",
          "joecolor": "b:c8ccbep:050604s:11110dt:2c2d29q:353630"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1701829495"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/86/a5/e4/86a5e4f4-d577-0776-32c7-934df4d927fd/mzaf_14051414921641954149.plus.aac.p.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/prada/1701829489?i=1701829495&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/prada/1701829489?i=1701829495&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "cass%C3%B6",
            "id": "42",
            "adamid": "1701835612"
          },
          {
            "alias": "raye",
            "id": "42",
            "adamid": "261686"
          },
          {
            "alias": "d-block-europe",
            "id": "42",
            "adamid": "1240341559"
          }
        ],
        "url": "https://www.shazam.com/track/668616600/prada",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1701835612/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1710430599",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1710430599?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "659236090",
        "title": "Daylight",
        "subtitle": "David Kushner",
        "share": {
          "subject": "Daylight - David Kushner",
          "text": "Daylight by David Kushner",
          "href": "https://www.shazam.com/track/659236090/daylight",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/29/b8/41/29b84156-2edb-692d-329b-8faf1d076054/23SYMIM06332.rgb.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Daylight by David Kushner.",
          "html": "https://www.shazam.com/snippets/email-share/659236090?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/71/b7/eb/71b7ebc4-537e-4122-5d91-13d0f77dc2b6/d423d5cb-b6e0-4462-9edb-dac6ee2a63c6_file_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/659236090"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/71/b7/eb/71b7ebc4-537e-4122-5d91-13d0f77dc2b6/d423d5cb-b6e0-4462-9edb-dac6ee2a63c6_file_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/29/b8/41/29b84156-2edb-692d-329b-8faf1d076054/23SYMIM06332.rgb.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/29/b8/41/29b84156-2edb-692d-329b-8faf1d076054/23SYMIM06332.rgb.jpg/400x400cc.jpg",
          "joecolor": "b:1b2500p:e7ecf5s:d1d3d8t:bec4c4q:adb0ad"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1702038908"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview126/v4/d9/3e/15/d93e150d-5c9f-b9a8-5dd1-4dc588cd691e/mzaf_16846279904485101440.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/daylight/1702038892?i=1702038908&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/daylight/1702038892?i=1702038908&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "david-kushner",
            "id": "42",
            "adamid": "1529375597"
          }
        ],
        "url": "https://www.shazam.com/track/659236090/daylight",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1529375597/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1681943558",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1681943558?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "668652025",
        "title": "ecstacy (slowed)",
        "subtitle": "SUICIDAL-IDOL",
        "share": {
          "subject": "ecstacy (slowed) - SUICIDAL-IDOL",
          "text": "ecstacy (slowed) by SUICIDAL-IDOL",
          "href": "https://www.shazam.com/track/668652025/ecstacy-slowed",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/fb/24/c3/fb24c3ae-fcb5-02bc-725d-7ad1009f421e/197876857118_Cover.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover ecstacy (slowed) by SUICIDAL-IDOL.",
          "html": "https://www.shazam.com/snippets/email-share/668652025?lang=en-US&country=US",
          "snapchat": "https://www.shazam.com/partner/sc/track/668652025"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/fb/24/c3/fb24c3ae-fcb5-02bc-725d-7ad1009f421e/197876857118_Cover.jpg/400x400cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/fb/24/c3/fb24c3ae-fcb5-02bc-725d-7ad1009f421e/197876857118_Cover.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music126/v4/fb/24/c3/fb24c3ae-fcb5-02bc-725d-7ad1009f421e/197876857118_Cover.jpg/400x400cc.jpg",
          "joecolor": "b:94a1b2p:120a09s:282020t:2c282bq:3e3a3d"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1696956489"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview126/v4/75/89/ca/7589ca2d-58ab-4640-93a8-f98ed4229368/mzaf_9649333284160425631.plus.aac.p.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/ecstacy-slowed/1696956481?i=1696956489&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/ecstacy-slowed/1696956481?i=1696956489&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "suicidal-idol",
            "id": "42",
            "adamid": "1610818819"
          }
        ],
        "url": "https://www.shazam.com/track/668652025/ecstacy-slowed",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1610818819/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "679889401",
        "title": "IDGAF (feat. Yeat)",
        "subtitle": "Drake",
        "share": {
          "subject": "IDGAF (feat. Yeat) - Drake",
          "text": "IDGAF (feat. Yeat) by Drake",
          "href": "https://www.shazam.com/track/679889401/idgaf-feat-yeat",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/21/50/ee/2150ee84-62c3-4190-7dfa-da30abd98ac8/23UM1IM09862.rgb.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover IDGAF (feat. Yeat) by Drake.",
          "html": "https://www.shazam.com/snippets/email-share/679889401?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages112/v4/1b/71/65/1b716513-b0c1-9c6b-45aa-cbb9198248cc/01607cf1-1d52-43d2-84ce-d6fc1f4a3475_ami-identity-7665599cc626d803af7956b3691905e7-2022-11-30T21-07-19.526Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/679889401"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages112/v4/1b/71/65/1b716513-b0c1-9c6b-45aa-cbb9198248cc/01607cf1-1d52-43d2-84ce-d6fc1f4a3475_ami-identity-7665599cc626d803af7956b3691905e7-2022-11-30T21-07-19.526Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/21/50/ee/2150ee84-62c3-4190-7dfa-da30abd98ac8/23UM1IM09862.rgb.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/21/50/ee/2150ee84-62c3-4190-7dfa-da30abd98ac8/23UM1IM09862.rgb.jpg/400x400cc.jpg",
          "joecolor": "b:010101p:c0d0d0s:c0c7c1t:99a7a7q:999f9b"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1710685786"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview126/v4/86/58/4d/86584df5-0815-7b5c-4dc8-091ee487340d/mzaf_10892088962516732142.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/idgaf-feat-yeat/1710685602?i=1710685786&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/idgaf-feat-yeat/1710685602?i=1710685786&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "drake",
            "id": "42",
            "adamid": "271256"
          }
        ],
        "url": "https://www.shazam.com/track/679889401/idgaf-feat-yeat",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/271256/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "10545594",
        "title": "Kozmic Blues",
        "subtitle": "Janis Joplin",
        "share": {
          "subject": "Kozmic Blues - Janis Joplin",
          "text": "Kozmic Blues by Janis Joplin",
          "href": "https://www.shazam.com/track/10545594/kozmic-blues",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music125/v4/8f/a2/b5/8fa2b508-23b9-0c2d-173c-9a009165d2f9/886445695687.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover Kozmic Blues by Janis Joplin.",
          "html": "https://www.shazam.com/snippets/email-share/10545594?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/Features125/v4/da/59/db/da59db5a-af59-3032-2ef2-bf0819bc3e80/mza_3577204118769306560.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/10545594"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/Features125/v4/da/59/db/da59db5a-af59-3032-2ef2-bf0819bc3e80/mza_3577204118769306560.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music125/v4/8f/a2/b5/8fa2b508-23b9-0c2d-173c-9a009165d2f9/886445695687.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music125/v4/8f/a2/b5/8fa2b508-23b9-0c2d-173c-9a009165d2f9/886445695687.jpg/400x400cc.jpg",
          "joecolor": "b:2e121ap:f6bf76s:f29550t:ce9c63q:cb7b45"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1073650453"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview115/v4/b7/db/dd/b7dbdd6b-184b-e220-e482-a4807eda8eee/mzaf_16727870563502679507.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/kozmic-blues/1073650135?i=1073650453&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/kozmic-blues/1073650135?i=1073650453&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "janis-joplin",
            "id": "42",
            "adamid": "365673"
          }
        ],
        "url": "https://www.shazam.com/track/10545594/kozmic-blues",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/365673/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "680060604",
        "title": "MONACO",
        "subtitle": "Bad Bunny",
        "share": {
          "subject": "MONACO - Bad Bunny",
          "text": "MONACO by Bad Bunny",
          "href": "https://www.shazam.com/track/680060604/monaco",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover MONACO by Bad Bunny.",
          "html": "https://www.shazam.com/snippets/email-share/680060604?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/1a/ac/74/1aac745b-0bce-18c5-d4ac-07dbf6192fee/5c71ee0e-11e3-42d8-b1f7-0bf850252dd9_ami-identity-b99c43259683c42629e6b3a66d65af95-2023-09-25T13-13-11.585Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/680060604"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/1a/ac/74/1aac745b-0bce-18c5-d4ac-07dbf6192fee/5c71ee0e-11e3-42d8-b1f7-0bf850252dd9_ami-identity-b99c43259683c42629e6b3a66d65af95-2023-09-25T13-13-11.585Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "joecolor": "b:daccc1p:080505s:332f2bt:322d2bq:554e49"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1710982872"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/23/72/dc/2372dcfe-5718-46b8-19a5-6c8915edb3f2/mzaf_6890668284608487807.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/monaco/1710982865?i=1710982872&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/monaco/1710982865?i=1710982872&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "bad-bunny",
            "id": "42",
            "adamid": "1126808565"
          }
        ],
        "url": "https://www.shazam.com/track/680060604/monaco",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1126808565/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1711726669",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1711726669?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "680060593",
        "title": "PERRO NEGRO",
        "subtitle": "Bad Bunny & Feid",
        "share": {
          "subject": "PERRO NEGRO - Bad Bunny & Feid",
          "text": "PERRO NEGRO by Bad Bunny & Feid",
          "href": "https://www.shazam.com/track/680060593/perro-negro",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover PERRO NEGRO by Bad Bunny & Feid.",
          "html": "https://www.shazam.com/snippets/email-share/680060593?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/1a/ac/74/1aac745b-0bce-18c5-d4ac-07dbf6192fee/5c71ee0e-11e3-42d8-b1f7-0bf850252dd9_ami-identity-b99c43259683c42629e6b3a66d65af95-2023-09-25T13-13-11.585Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/680060593"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/1a/ac/74/1aac745b-0bce-18c5-d4ac-07dbf6192fee/5c71ee0e-11e3-42d8-b1f7-0bf850252dd9_ami-identity-b99c43259683c42629e6b3a66d65af95-2023-09-25T13-13-11.585Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "joecolor": "b:daccc1p:080505s:332f2bt:322d2bq:554e49"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1710983279"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/78/5d/13/785d13ee-bcbc-9ce2-19fa-de5551eea152/mzaf_13583290346849520512.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/perro-negro/1710982865?i=1710983279&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/perro-negro/1710982865?i=1710983279&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "bad-bunny",
            "id": "42",
            "adamid": "1126808565"
          },
          {
            "alias": "feid",
            "id": "42",
            "adamid": "194502100"
          }
        ],
        "url": "https://www.shazam.com/track/680060593/perro-negro",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1126808565/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "672088996",
        "title": "My Love Mine All Mine",
        "subtitle": "Mitski",
        "share": {
          "subject": "My Love Mine All Mine - Mitski",
          "text": "My Love Mine All Mine by Mitski",
          "href": "https://www.shazam.com/track/672088996/my-love-mine-all-mine",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/8f/4b/40/8f4b4044-d02b-dc25-7d01-7d9d225fdce4/40240.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover My Love Mine All Mine by Mitski.",
          "html": "https://www.shazam.com/snippets/email-share/672088996?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/33/74/c9/3374c966-953a-ec66-d654-8f1015f45315/65c7d549-a28f-42f8-bfd5-d65f6d4c040d_ami-identity-357f46d0e21d950755075f1c9ac90d04-2023-07-26T16-05-08.789Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/672088996"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages126/v4/33/74/c9/3374c966-953a-ec66-d654-8f1015f45315/65c7d549-a28f-42f8-bfd5-d65f6d4c040d_ami-identity-357f46d0e21d950755075f1c9ac90d04-2023-07-26T16-05-08.789Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/8f/4b/40/8f4b4044-d02b-dc25-7d01-7d9d225fdce4/40240.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/8f/4b/40/8f4b4044-d02b-dc25-7d01-7d9d225fdce4/40240.jpg/400x400cc.jpg",
          "joecolor": "b:fae2d2p:020202s:1e1e1et:332f2bq:4a4542"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1697335814"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/17/c6/ab/17c6ab2d-2221-a445-0062-8624e3438618/mzaf_4936030855212727653.plus.aac.p.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/my-love-mine-all-mine/1697335341?i=1697335814&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/my-love-mine-all-mine/1697335341?i=1697335814&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": false,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "mitski",
            "id": "42",
            "adamid": "497546276"
          }
        ],
        "url": "https://www.shazam.com/track/672088996/my-love-mine-all-mine",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/497546276/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web&videoIdToFilter=1707040767",
          "trackhighlighturl": "https://cdn.shazam.com/video/v3/en-US/US/web/highlights/1707040767?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      },
      {
        "layout": "5",
        "type": "MUSIC",
        "key": "680060591",
        "title": "BABY NUEVA",
        "subtitle": "Bad Bunny",
        "share": {
          "subject": "BABY NUEVA - Bad Bunny",
          "text": "BABY NUEVA by Bad Bunny",
          "href": "https://www.shazam.com/track/680060591/baby-nueva",
          "image": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "twitter": "I used @Shazam to discover BABY NUEVA by Bad Bunny.",
          "html": "https://www.shazam.com/snippets/email-share/680060591?lang=en-US&country=US",
          "avatar": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/1a/ac/74/1aac745b-0bce-18c5-d4ac-07dbf6192fee/5c71ee0e-11e3-42d8-b1f7-0bf850252dd9_ami-identity-b99c43259683c42629e6b3a66d65af95-2023-09-25T13-13-11.585Z_cropped.png/800x800cc.jpg",
          "snapchat": "https://www.shazam.com/partner/sc/track/680060591"
        },
        "images": {
          "background": "https://is1-ssl.mzstatic.com/image/thumb/AMCArtistImages116/v4/1a/ac/74/1aac745b-0bce-18c5-d4ac-07dbf6192fee/5c71ee0e-11e3-42d8-b1f7-0bf850252dd9_ami-identity-b99c43259683c42629e6b3a66d65af95-2023-09-25T13-13-11.585Z_cropped.png/800x800cc.jpg",
          "coverart": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "coverarthq": "https://is1-ssl.mzstatic.com/image/thumb/Music116/v4/13/21/22/132122a1-2ef2-381b-94b6-7b9449dcaa4a/197190137897.jpg/400x400cc.jpg",
          "joecolor": "b:daccc1p:080505s:332f2bt:322d2bq:554e49"
        },
        "hub": {
          "type": "APPLEMUSIC",
          "image": "https://images.shazam.com/static/icons/hub/web/v5/applemusic.png",
          "actions": [
            {
              "name": "apple",
              "type": "applemusicplay",
              "id": "1710983204"
            },
            {
              "name": "apple",
              "type": "uri",
              "uri": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/ac/44/5e/ac445e0e-1880-4dab-0b88-78cf8fd5a0cd/mzaf_17598672376748335232.plus.aac.ep.m4a"
            }
          ],
          "options": [
            {
              "caption": "OPEN",
              "actions": [
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "applemusicopen",
                  "uri": "https://music.apple.com/us/album/baby-nueva/1710982865?i=1710983204&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                },
                {
                  "name": "hub:applemusic:deeplink",
                  "type": "uri",
                  "uri": "https://music.apple.com/us/album/baby-nueva/1710982865?i=1710983204&mttnagencyid=s2n&mttnsiteid=125115&mttn3pid=Apple-Shazam&mttnsub1=Shazam_web&mttnsub2=5348615A-616D-3235-3830-44754D6D5973&itscg=30201&app=music&itsct=Shazam_web"
                }
              ],
              "beacondata": {
                "type": "open",
                "providername": "applemusic"
              },
              "image": "https://images.shazam.com/static/icons/hub/web/v5/overflow-open-option.png",
              "type": "open",
              "listcaption": "Open in Apple Music",
              "overflowimage": "https://images.shazam.com/static/icons/hub/web/v5/applemusic-overflow.png",
              "colouroverflowimage": false,
              "providername": "applemusic"
            }
          ],
          "explicit": true,
          "displayname": "APPLE MUSIC"
        },
        "artists": [
          {
            "alias": "bad-bunny",
            "id": "42",
            "adamid": "1126808565"
          }
        ],
        "url": "https://www.shazam.com/track/680060591/baby-nueva",
        "highlightsurls": {
          "artisthighlightsurl": "https://cdn.shazam.com/video/v3/en-US/US/web/1126808565/highlights?affiliate=mttnagencyid%3Ds2n%26mttnsiteid%3D125115%26mttn3pid%3DApple-Shazam%26mttnsub1%3DShazam_web%26mttnsub2%3D5348615A-616D-3235-3830-44754D6D5973%26itscg%3D30201%26app%3Dmusic%26itsct%3DShazam_web"
        },
        "properties": {}
      }
    ]
  }}