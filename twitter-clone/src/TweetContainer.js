import styled from 'styled-components';

const TweetContainer = styled.div`
  // your styles here
  padding: 20px;
  border: 1px solid #e1e8ed;
  border-radius: 8px;
  margin-bottom: 10px;
`;
