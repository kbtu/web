import React, { useState } from 'react';
import TweetBox from './TweetBox';
import EditTweet from './EditTweet';

const initialTweets = [
  { id: 1, content: 'This is the first tweet', likes: 0, dislikes: 0 },
  { id: 2, content: 'This is the second tweet', likes: 0, dislikes: 0 },
  // ...
];

const App = () => {
  const [tweets, setTweets] = useState(initialTweets);

  const handleTweetPost = (content) => {
    const newTweet = {
      id: Date.now(), // simplistic approach for example purposes
      content,
    };
    setTweets([newTweet, ...tweets]);
  };

  const handleUpdateTweet = (id, updatedContent) => {
    setTweets(
      tweets.map((tweet) =>
        tweet.id === id ? { ...tweet, content: updatedContent } : tweet
      )
    );
  };

  const handleLike = (id) => {
    setTweets(
      tweets.map((tweet) =>
        tweet.id === id ? { ...tweet, likes: tweet.likes + 1 } : tweet
      )
    );
  };
  
  const handleDislike = (id) => {
    setTweets(
      tweets.map((tweet) =>
        tweet.id === id ? { ...tweet, dislikes: tweet.dislikes + 1 } : tweet
      )
    );
  };

  return (
    <div>
      <TweetBox onTweetPost={handleTweetPost} />
      {tweets.map((tweet) => (
        <EditTweet
          key={tweet.id}
          tweet={tweet}
          onUpdateTweet={handleUpdateTweet}
          onLike={handleLike}
          onDislike={handleDislike}
        />
      ))}
    </div>
  );
};

export default App;
