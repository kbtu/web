import React, { useState } from 'react';
import styled from 'styled-components';

// Reuse styled components from TweetBox.js
// ...

const TweetContainer = styled.div`
  padding: 20px;
  border: 1px solid #e1e8ed;
  border-radius: 8px;
  margin-bottom: 10px;
  // more styles
`;


const TweetInput = styled.textarea`
  width: 100%;
  padding: 10px 15px;
  margin-bottom: 10px;
  border-radius: 20px;
  border: 1px solid #e1e8ed;
  resize: none;
`;

const TweetButton = styled.button`
  padding: 10px 20px;
  border-radius: 20px;
  border: none;
  background-color: #1da1f2;
  color: white;
  cursor: pointer;
  &:hover {
    background-color: #1991db;
  }
`;
// Inside EditTweet.js or where you define your tweet display component

// ...

const TweetActions = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 0;
`;

const ActionButton = styled.button`
  // Your styling for the buttons
  padding: 10px 20px;
  border-radius: 20px;
  border: none;
  background-color: #1de2f2;
  color: white;
  cursor: pointer;
  &:hover {
    background-color: #1991db;
`;

const EditTweet = ({ tweet, onUpdateTweet, onLike, onDislike }) => {
  const [editContent, setEditContent] = useState(tweet.content);

  const updateTweet = () => {
    // Placeholder for a function to update the tweet
    onUpdateTweet(tweet.id, editContent);
  };

  return (
    <TweetContainer>
      <TweetInput
        value={editContent}
        onChange={(e) => setEditContent(e.target.value)}
      />
      <TweetButton onClick={updateTweet}>Update Tweet</TweetButton>
      <TweetActions>
        <ActionButton onClick={() => onLike(tweet.id)}>
          Like ({tweet.likes})
        </ActionButton>
        <ActionButton onClick={() => onDislike(tweet.id)}>
          Dislike ({tweet.dislikes})
        </ActionButton>
      </TweetActions>
    </TweetContainer>
  );
};

export default EditTweet;


