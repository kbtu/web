import React, { useState } from 'react';
import styled from 'styled-components';

const TweetContainer = styled.div`
  padding: 20px;
  border: 1px solid #e1e8ed;
  border-radius: 8px;
  margin-bottom: 10px;
`;

const TweetInput = styled.textarea`
  width: 100%;
  padding: 10px 15px;
  margin-bottom: 10px;
  border-radius: 20px;
  border: 1px solid #e1e8ed;
  resize: none;
`;

const TweetButton = styled.button`
  padding: 10px 20px;
  border-radius: 20px;
  border: none;
  background-color: #1da1f2;
  color: white;
  cursor: pointer;
  &:hover {
    background-color: #1991db;
  }
`;

const TweetBox = ({ onTweetPost }) => {
  const [tweetContent, setTweetContent] = useState('');

  const postTweet = () => {
    // Placeholder for a function to post the tweet
    onTweetPost(tweetContent);
    setTweetContent(''); // Clear the input after posting
  };

  return (
    <TweetContainer>
      <TweetInput
        value={tweetContent}
        onChange={(e) => setTweetContent(e.target.value)}
        placeholder="What's happening?"
      />
      <TweetButton onClick={postTweet}>Tweet</TweetButton>
    </TweetContainer>
  );
};

export default TweetBox;

