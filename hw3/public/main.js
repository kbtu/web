type Base = { onClick: () => void };
export const withLoggingOnClickWithParams = <TProps extends Base>(
  Component: ComponentType<TProps>,
  // adding some params as a second argument to the function
  params: { text: string },
) => {
  return (props: TProps) => {
    const onClick = () => {
      // accessing params that we passed as an argument here
      // everything else stays the same
      console.log('Log on click: ', params.text);
      props.onClick();
    };

    return <Component {...props} onClick={onClick} />;
  };
};
