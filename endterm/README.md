
# Spring Boot Posts Application

## Overview
This Spring Boot application provides a RESTful API for managing posts. It includes basic CRUD (Create, Read, Update, Delete) operations. The application is built using Spring Web, Spring Data JPA, and an H2 in-memory database.

## Features
- Create new posts
- Retrieve all posts
- Retrieve a single post by ID
- Update an existing post
- Delete a post

## Getting Started
### Prerequisites
- JDK 1.8 or later
- Maven 3.2+

### Running the Application
1. Clone the repository:
   ```
   git clone [repository URL]
   ```
2. Navigate to the directory:
   ```
   cd [project directory]
   ```
3. Run the application:
   ```
   mvn spring-boot:run
   ```

## API Endpoints
### Create a New Post
- **URL**: `/posts`
- **Method**: `POST`
- **Body**:
  ```json
  {
    "title": "Post Title",
    "text": "Post content",
    "author": "Author Name"
  }
  ```

### Get All Posts
- **URL**: `/posts`
- **Method**: `GET`

### Get a Single Post
- **URL**: `/posts/{id}`
- **Method**: `GET`

### Update a Post
- **URL**: `/posts/{id}`
- **Method**: `PUT`
- **Body**:
  ```json
  {
    "title": "Updated Title",
    "text": "Updated content",
    "author": "Author Name"
  }
  ```

### Delete a Post
- **URL**: `/posts/{id}`
- **Method**: `DELETE`

## Built With
- [Spring Boot](https://spring.io/projects/spring-boot) - The web framework used
- [Maven](https://maven.apache.org/) - Dependency Management
- [H2 Database](https://www.h2database.com/html/main.html) - In-memory database

## Authors
- Makhambet Torezhan

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments
- For endterm Web Programming Course at KBTU Master Class