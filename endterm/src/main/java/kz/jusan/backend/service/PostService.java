package kz.jusan.backend.service;

import kz.jusan.backend.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostService {
    @Autowired
    private PostRepository postRepository;

    // Methods for CRUD operations
    public Post savePost(Post post) {
        return postRepository.save(post);
    }
}
